import React from 'React'
import ReactDOM from 'react-dom'
import InText from './input'
import styled from 'styled-components'
// export default function List(props) {
//   return (import React from 'react';
const H1 = styled.h1`
  font-size: 1.5em;
  text-align: center;
  color: ${props => (props.redFont ? 'red' : 'black')};
`

export default class Home extends React.Component {
  render() {
    return (
      <div>
        <H1 redFont>欢迎</H1>
        <a href="#/detail" redFont>
          去detail
        </a>
      </div>
    )
  }
}
//     <ul id={f}>
//       <li></li>
//     </ul>
//   )
// }
