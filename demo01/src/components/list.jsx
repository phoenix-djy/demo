import React from 'react'
import styled from 'styled-components'
const H1 = styled.h2`
  font-size: 1.5em;
  text-align: center;
  color: ${props => (props.redFont ? 'red' : 'black')};
`
export default class todoList extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      list: []
    }
  }

  keyEnter = e => {
    if (e.which === 13) {
      let ele = e.target
      let listinfo = {
        title: ele.value,
        isck: false
      }
      let newlist = this.state.list
      newlist.push(listinfo)
      this.setState({
        list: newlist
      })
      ele.value = ''
    }
  }

  changeBox = index => {
    let data = this.state.list
    data[index].isck = !data[index].isck
    this.setState({
      list: data
    })
  }
  deleteData = index => {
    let data = this.state.list
    data.splice(index, 1)
    this.setState({
      list: data
    })
  }
  render() {
    return (
      <div className="block">
        <div className="title">
          <div className="titleleft">
            <H1 redFont>待办事项</H1>
          </div>
          <div className="titleright">
            <input type="text" onKeyPress={this.keyEnter} />
          </div>
        </div>
        <hr />
        <div>
          <ul>
            <h3>未完成</h3>
            {this.state.list.map((v, k) => {
              if (!v.isck) {
                return (
                  <div key={k}>
                    <li>
                      <input
                        type="checkbox"
                        checked={v.isck}
                        onChange={this.changeBox.bind(this, k)}
                      />
                      {v.title}
                      <button onClick={this.deleteData.bind(this, k)}>
                        删除
                      </button>
                    </li>
                  </div>
                )
              }
              return ''
            })}
            <h3>已完成</h3>
            {this.state.list.map((v, k) => {
              if (v.isck) {
                return (
                  <div key={k}>
                    <li>
                      <input
                        type="checkbox"
                        checked={v.isck}
                        onChange={this.changeBox.bind(this, k)}
                      />
                      {v.title}
                      <button onClick={this.deleteData.bind(this, k)}>
                        删除
                      </button>
                    </li>
                  </div>
                )
              }
              return ''
            })}
          </ul>
        </div>
      </div>
    )
  }
}
