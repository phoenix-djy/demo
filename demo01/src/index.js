import React from 'react'
import ReactDOM from 'react-dom'

// const mydiv = React.createElement('div', {}, '第一次用react')
//定义备忘录输入框
import InText from './components/input'
import List from './components/list'
import Router from './router/router'

ReactDOM.render(<Router />, document.getElementById('box'))
