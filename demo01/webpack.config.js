const path = require('path')
const HtmlWebPackPlugin = require('html-webpack-plugin')

//创建一个插件的实例对象
const htmlPlugin = new HtmlWebPackPlugin({
  template: path.join(__dirname, './src/index.html'),
  filename: 'index.html'
})
//webpack默认只能打包处理。js文件  其他文件只能交由第三方的loader管理

//向往外面暴露一个打包的配置对象 node语法 怕、webpack死基于node构建的
module.exports = {
  mode: 'development', //production
  plugins: [htmlPlugin],
  //在webpack4.x中  约定大于配置 约定  默认的的打包入口为src———>index。js
  module: {
    //所有的第三方的配置规则
    rules: [
      {
        test: /\.js|jsx$/,
        use: 'babel-loader',
        exclude: /node_modules/
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader?modules']
      },
      {
        test: /\.ttf|woff|woff2|eot|svg$/,
        use: 'url-loader'
      }
    ]
  },
  resolve: {
    extensions: ['.js', '.jsx', '.json'], //表示这几个文件的后缀名可以省略不写
    alias: {
      '@': path.join(__dirname, './src') //@代表项目根目录中src的这一层路径
    }
  }
}
